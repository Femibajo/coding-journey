
const calcTip = bill => {
    return bill >= 50 && bill <=300 ? bill * 0.15 : bill * 0.20;
    
} //  arrow function to calculate tip based on set condition

const bills = [ 125, 555, 44]; // test data
const tips = new Array(); // declaring new array 'tips'
const Total = new Array();


    for (let i = 0; i < bills.length; i++) {
        tips[i] = calcTip(bills[i]);
        Total[i] = tips[i] + bills[i];
}     // looping through values of array bills to compute corresponding values for other arrays

console.log(bills);
console.log(tips);
console.log(Total);


